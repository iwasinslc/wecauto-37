@extends('layouts.auth')
@section('title', __('Marketing'))
@section('content')
    <section class="page-preview">
        <div class="container">
            <div class="breadcrumbs"><a href="{{route('customer.main')}}">{{__('Home')}}</a><span>/ </span><span>{{__('Marketing')}}</span>
            </div>
            <h1 class="page-preview__title">{{__('Marketing')}}
            </h1>
        </div>
    </section>
    <section class="about-info">
        <div class="container">
            <div class="typography">
                <blockquote>
                <p>{{__('A client is allowed to take part in the WECAUTO project if he buys a license and makes an initial payment of 35% of the selected car, motorcycle or special equipment cost. Auto deposit is valid for 12 months from the first payment date. After this period expiration, the client gets the entire amount to purchase a car or motor vehicles.')}}</p>
                </blockquote>
                <h4 class="color-warning text-uppercase"><p>{{__('License purchase')}}</p> </h4>
                <p>{{__("License purchase allows the client to become a full member of the auto deposit system. There are 7 licenses to choose from that have different validity periods, different car buy limits, as well as different Faster (FST) buy and sale limits. FST speeds up the process of car, motorcycle or special vehicle purchasing.")}}</p>
            </div>
            <div class="licenses">
                <div class="js-swiper-licenses swiper-container swiper-no-swiping">
                    <div class="swiper-wrapper">
                        @foreach($licences as $licence)
                            <div class="swiper-slide">
                                <div class="license-item">
                                    <p class="license-item__name">{{__($licence->name)}} {{$licence->id}}
                                    </p>
                                    <div class="license-item__price">
                                        <div class="license-item__price-count"><span><span class="color-warning">$</span>{{$licence->price}}</span>
                                        </div>
                                    </div>

                                    <p class="license-item__subtitle">{!! __('Duration') !!} — <span class="color-warning">{{$licence->duration}} {!! __('days') !!}</span>
                                    </p>

                                    <ul class="license-item__list">
                                        <li>{!! __('Price') !!} -  <strong> {{$licence->price*rate('USD', $licence->currency->code)}} {{$licence->currency->code}}  </strong>
                                        </li>
                                        <li>{!! __('Minimum amount to receive') !!} - <strong>${{$licence->deposit_min}}</strong></li>
                                        <li>{!! __('Maximum amount to receive') !!} - <strong>${{$licence->deposit_max}}</strong></li>
                                        <li>{!! __('Maximum purchase amount FST') !!} <strong>${{$licence->buy_amount}}</strong>
                                        </li>
                                        <li>{!! __('Maximum FST Sale Amount') !!} <strong>${{$licence->sell_amount}}</strong>
                                        </li>
                                    </ul>
                                    {{--                            <div class="license-item__desc">--}}
                                    {{--                                <p>Car price $7000</p>--}}
                                    {{--                                <p><small>(max)</small></p>--}}
                                    {{--                            </div>--}}
                                    <div class="license-item__buttons">
                                            <a href="{{route('login')}}" class="btn js-modal">{{__('Activate')}}
                                            </a>


                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                    <div class="dots-pagination">  </div>
                </div>
                <div class="licenses__note">
                    <p>{{__('Each license allows you to place one order to buy a car, motorcycle and or special vehicle.')}}</p>
                </div>
            </div>
        </div>
    </section>
    <section class="fasters">
        <div class="composition-fasters" data-emergence="hidden"><img class="composition-fasters__cubes" src="/assets/images/composition-fasters.png" alt="" role="presentation"/><img class="composition-fasters__car" src="/assets/images/fasters-car.png" alt="" role="presentation"/><img class="composition-fasters__fst" src="/assets/images/fst2.png" alt="" role="presentation"/>
        </div>
        <div class="container">
            <div class="fasters__row">
                <div class="fasters__col">
                    <div class="typography">
                        <p> <strong>
                                <big><p>{{__('The deposit period is 12 months; however, you can shorten it up to 4 months using the FSTs.')}}</big></strong></p>
                        <p>{{__("To shorten the deposit period as much as possible, you need to purchase the fasters for a total amount of 15% of the declared car cost in USD. Fasters are used exclusively to approach the fund receiving period to purchase a car. FST can be purchased and activated in parts, while each share reduces the days in proportion to its cost (all information is displayed in your personal account and is always up-to-date).")}}</p>
                    </div>
                </div>
                <div class="fasters__col">
                    <h4 class="title-arrow"> <span><p>{{__('FST daily sale limits')}}</p></span>
                    </h4>
                    <div class="info-block">
                        <h5 class="info-block__title"> <span><p>{{__('FSTs are sold based on the p2p principle according to the daily limits set for each rank within the total active license limits. FSTs are sold on the WECAUTO internal exchange')}}</p></span>
                        </h5>
                        <div class="typography">
                            <p> <i><p>{{__('For information on daily sale limits, see the "Affiliate program and ranks" tab')}}</p></i></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="gray-block">
        <div class="container">
            <div class="gray-block__block">
                <div class="gray-block__row">
                    <div class="gray-block__col">
                        <div class="rhombus-item">
                            <div class="rhombus-item__icon">
                                <svg class="svg-icon">
                                    <use href="/assets/icons/sprite.svg#icon-website"></use>
                                </svg>
                            </div>
                            <p class="rhombus-item__content"><p>{{__('Faster growth algorithms')}}</p>
                            </p>
                        </div>
                        <div class="typography">
                            <p>{{__("The faster cost directly depends on all participants’ total deposit amount to the auto program.")}}</p>
                            <p>{{__('Every $10 million invested by partners in the auto deposits doubles the faster costs.')}}</p>
                       </div>
                    </div>
                    <div class="gray-block__col">
                        <div class="rhombus-item">
                            <div class="rhombus-item__icon">
                                <svg class="svg-icon">
                                    <use href="/assets/icons/sprite.svg#icon-shuttle"></use>
                                </svg>
                            </div>
                            <p class="rhombus-item__content"><p>{{__('How to get the fasters?')}}</p>
                            </p>
                        </div>
                        <div class="typography">
                            <p> <strong>{{__('There are 5 ways to get the FSTs, namely:')}}</strong></p>
                            <ol class="circle-number-list-small">
                                <li>{{__("buy a license (a cashback is made in FST in the amount of 3% of the purchase amount);")}} </li>
                                <li>{{__('participate in the affiliate program and invite other participants to the project;')}} </li>
                                <li>{{__('buy FST from other participants on the P2P exchange;')}} </li>
                                <li>{{__('convert ACC and GNT to FST in your personal account at the website rate at the conversion time;')}}</li>
                                <li>{{__('provide documents and a video report of car purchasing with the help of our system.')}}</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="partner-program">
        <div class="container">
            <h3 class="section-title"><span class="color-warning"><p>{{__('Affiliate program and ranks')}}</p>
            </h3>
            <p class="partner-program__subtitle"><p>{{__('The WECAUTO project has a 10-level referral system')}}</p>
            </p>
            <div class="refferal-levels">
                <ul>
                    <li>
                        <div class="refferal-level">
                            <big>1 </big><span>{{__('level')}}</span>
                        </div>
                        <div class="refferal-percent">10<sup>%</sup>
                        </div>
                    </li>
                    <li>
                        <div class="refferal-level">
                            <big>2 </big><span>{{__('level')}}</span>
                        </div>
                        <div class="refferal-percent">5<sup>%</sup>
                        </div>
                    </li>
                    <li>
                        <div class="refferal-level">
                            <big>3 </big><span>{{__('level')}}</span>
                        </div>
                        <div class="refferal-percent">3<sup>%</sup>
                        </div>
                    </li>
                    <li>
                        <div class="refferal-level">
                            <big>4 </big><span>{{__('level')}}</span>
                        </div>
                        <div class="refferal-percent">3<sup>%</sup>
                        </div>
                    </li>
                    <li>
                        <div class="refferal-level">
                            <big>5 </big><span>{{__('level')}}</span>
                        </div>
                        <div class="refferal-percent">3<sup>%</sup>
                        </div>
                    </li>
                    <li>
                        <div class="refferal-level">
                            <big>6 </big><span>{{__('level')}}</span>
                        </div>
                        <div class="refferal-percent">3<sup>%</sup>
                        </div>
                    </li>
                    <li>
                        <div class="refferal-level">
                            <big>7 </big><span>{{__('level')}}</span>
                        </div>
                        <div class="refferal-percent">3<sup>%</sup>
                        </div>
                    </li>
                    <li>
                        <div class="refferal-level">
                            <big>8 </big><span>{{__('level')}}</span>
                        </div>
                        <div class="refferal-percent">3<sup>%</sup>
                        </div>
                    </li>
                    <li>
                        <div class="refferal-level">
                            <big>9 </big><span>{{__('level')}}</span>
                        </div>
                        <div class="refferal-percent">5<sup>%</sup>
                        </div>
                    </li>
                    <li>
                        <div class="refferal-level">
                            <big>10 </big><span>{{__('level')}}</span>
                        </div>
                        <div class="refferal-percent">10<sup>%</sup>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="partner-program__row">
                <div class="partner-program__col">
                    <div class="typography">
                        <p> <strong>
                                <big>{{__("At level 1, the referral bonus is 10% of the referral's license cost; at level 2-5%; at level 3 - 3%; at level 4 - 3%; at level 5 - 3%; at level 6 - 3%; at level 7 - 3%; at level 8 - 3%; at level 9 - 5%; at level 10 - 10%.")}}</big></strong></p>
                        <p>{{__('Affiliate rewards are credited in FST. The affiliate program provides 7 ranks:')}}</p>
                        <ul class="before-warning">
                            <li> <strong>{{__('Partner;')}} </strong></li>
                            <li> <strong>{{__('Manager;')}} </strong></li>
                            <li> <strong>{{__('Director;')}} </strong></li>
                            <li> <strong>{{__('President;')}} </strong></li>
                            <li> <strong>{{__('Shareholder;')}}</strong></li>
                            <li> <strong>{{__('Shareholder**;')}}</strong></li>
                            <li> <strong>{{__('Shareholder***.')}}</strong></li>


                        </ul>
                        <p>{{__('The "Partner" status is assigned to any member who has activated any cost license.')}}</p>
                        <p>{{__('To upgrade the “Partner” status up to “Manager” status, you need to purchase a license worth at least $480, as well as to have at least 3 personally invited participants in your network who have activated any license and made a deposit to buy a car or motorcycle.')}} </p>
                        <p>{{__('At the "Manager" rank, 2 affiliate program levels are available.')}}</p>
                        <p>{{__('At the "Director" rank 3 levels of the affiliate program are available, you need to have at least 2 participants in the "Manager" rank in your first line, as well as an active license worth at least $2,300.')}}</p>
                        <p>{{__('The "President" rank is assigned to a participant who has at least 2 participants in the "Director" rank in his first line, as well as an active license worth at least $9,000.In the "President" rank, 4 and 5 affiliate program levels are available.')}} </p>
                        <p>{{__('"Shareholder" rank is available if the client has a license worth at least $22,000 and at least 2 participants in the "President" rank in the first line.')}}</p>
                         <p>{{__('The "Shareholder **" rank is assigned to a participant if he has a license worth at least $42,000 and at least 2 partners in the "Shareholder" rank in the first line.')}}</p>
                            <p>{{__('The highest "Shareholder ***" rank is given to a participant who has at least two partners in his first line in the "Shareholder **" rank and a license of the highest level. Participants with the "Shareholder", "Shareholder **" and "Shareholder ***" ranks have access to all levels of the referral system.')}}</p>
                       <p>{{__('When purchasing a subsequent license below the license cost that corresponds to the assigned rank, referral accruals, as well as deposit limits and FST buy/sale limits will change according to the newly purchased license terms.')}}</p>
                    </div>
                </div>
                <div class="partner-program__col">
                    <div class="partner-program__image"><img src="/assets/images/become.png" alt="Become our partner">
                    </div>
                    <div class="info-block">
                        <h5 class="info-block__title"> <span> <p>{{__('FST daily sale limits for each rank:')}}</p> </span>
                        </h5>
                        <div class="typography">
                            <p>{{__('Partner Rank: FST daily sale limit - 1.5%')}}</p>
                            <p>{{__('Manager Rank: FST daily sale limit - 3.0%')}}</p>
                            <p>{{__('Director Rank: FST daily sale limit - 4.5%')}}</p>
                            <p>{{__('President Rank: FST daily sale limit - 7.5%')}}</p>
                            <p>{{__('Shareholder Rank: FST daily sale limit - 15.0%')}}</p>
                            <p>{{__('Shareholder** Rank: FST daily sale limit - 22.5%')}}</p>
                            <p>{{__('Shareholder*** Rank: FST daily sale limit - 30%')}}</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection