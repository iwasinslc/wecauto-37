<?php

namespace App\Http\Controllers\Api\BotApi;

use App\Models\User;
use Illuminate\Validation\ValidationException;

class CustomerController
{
    /**
     * @param $myId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function show($myId)
    {
        \Validator::validate([
            'my_id' => $myId
        ], [
            'my_id' => 'required|string|numeric'
        ]);

        $customer = User::query()
            ->with('partner')
            ->where('my_id', $myId)
            ->first();

        if (!$customer) {
            throw ValidationException::withMessages([
                'my_id' => 'Customer not found'
            ]);
        }

        return response()->json([
            'data' => [
                'login' => $customer->login,
                'my_id' => $customer->my_id,
                'partner' => $customer->partner ? [
                    'login' => $customer->partner->login,
                    'my_id' => $customer->partner->my_id
                ] : null
            ]
        ]);
    }
}
