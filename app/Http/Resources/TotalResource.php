<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *     title="TotalResource",
 *     description="Total resource",
 *      @OA\Property(
 *         property="totals",
 *         title="Totals wrapper",
 *         description="Totals wrapper",
 *         @OA\Property(property="currency_code", ref="#/components/schemas/Total")
 *      )
 * )
 */
class TotalResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        try {
            $totals = [
                'withdraws' => getUserTotalWithdrawn(false, $this),
                'deposited' => getUserTotalDeposited(false, $this),
                'transfers_send' => getUserTotalSendTransfer(false, $this),
                'transfers_received' => getUserTotalReceivedTransfer(false, $this),
                'partner' => getUserTotalPartner(false, $this),
                'earned' => getUserTotalEarned(false, $this),
                'bonus' => getUserTotalBonus(false, $this),
                'charity' => getUserTotalCharity(false, $this)
            ];
            $totalsFormat = [];
            foreach($totals as $key => $key_values) {
                foreach ($key_values as $currency => $value) {
                    $totalsFormat[$currency][$key] = $value;
                }
            }
        } catch (\Exception $e) {
            $totalsFormat = null;
        }

        return $totalsFormat;
    }
}
