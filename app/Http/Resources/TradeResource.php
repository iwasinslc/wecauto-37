<?php

namespace App\Http\Resources;

use App\Models\OrderPiece;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *     title="TradeResource",
 *     description="Trade resource",
 *     type="object",
 *     @OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/Trade"))
 * )
 */
class TradeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var OrderPiece $this */
        return [
            'id' => $this->id,
            'type' => $this->type,
            'value' => $this->amount,
            'currency' => $this->mainCurrency->code,
            'rate' => $this->rate,
            'rate_currency' => $this->currency->code,
            'rate_amount' => $this->amount * $this->rate,
            'limit_amount' => $this->limit_amount,
            'limit_currency' => 'USD',
            'fee' => $this->fee,
            'order_id' => $this->order_id,
            'created_at' => $this->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $this->updated_at->format('Y-m-d H:i:s')
        ];
    }
}
