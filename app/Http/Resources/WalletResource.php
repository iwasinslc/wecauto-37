<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *     title="WalletResource",
 *     description="Wallet resource",
 *      @OA\Property(
 *         property="wallets",
 *         title="Wallets wrapper",
 *         type="array",
 *         description="Wallets wrapper",
 *         @OA\Items(ref="#/components/schemas/Wallet")
 *      )
 * )
 */
class WalletResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'balance' => $this->balance,
            'currency' => $this->currency->code,
            'payment_system' => ucfirst($this->paymentSystem->name),
            'address' => $this->address
        ];
    }
}
