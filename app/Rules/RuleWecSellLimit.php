<?php
namespace App\Rules;

use App\Models\Currency;
use App\Models\ExchangeOrder;
use App\Models\OrderPiece;
use App\Models\Setting;
use App\Models\Transaction;
use App\Models\TransactionType;
use App\Models\Wallet;
use Illuminate\Contracts\Validation\Rule;


/**
 * Class RuleWecSellLimit
 * @package App\Rules
 */
class RuleWecSellLimit implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  float  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {


        if(request()->type==ExchangeOrder::TYPE_BUY)  return true;



        /**
         * @var Wallet $mainWallet
         */
        $mainWallet = user()->wallets()->find(request()->main_wallet_id);


        if ($mainWallet->currency->code!='WEC') return true;

        /**
         * @var Wallet $wallet
         */
        $wallet = user()->wallets()->find(request()->wallet_id);


        $amount = $value*request()->rate*rate($wallet->currency->code, 'USD');

        $limit = user()->deposit_sell_limit;



        return $limit>=$amount;



    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Exceeded max sell limit');
    }
}
