<?php
namespace App\Rules;

use App\Models\Currency;
use App\Models\ExchangeOrder;
use App\Models\OrderPiece;
use App\Models\Setting;
use App\Models\Transaction;
use App\Models\TransactionType;
use App\Models\Wallet;
use Illuminate\Contracts\Validation\Rule;


/**
 * Class RulePlanRange
 * @package App\Rules
 */
class RuleWeekDepositLimit implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  float  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {


        if (user()->representative==1)
        {
            return true;
        }




        $values = [$value];

        if ($value==1||$value==2)
        {
            $values = [1,2];
            $limit =  (integer)Setting::getValue('moto_limit');
        }
        elseif($value==0)
        {
            $limit =  (integer)Setting::getValue('auto_limit');
        }
        else
        {
            $limit =  (integer)Setting::getValue('spec_limit');
        }






        $count = getDepositsCount(null, $values, now()->startOfWeek(), now(), 0);


        return $limit>=$count;



    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Weekly buy limit has been reached');
    }
}
