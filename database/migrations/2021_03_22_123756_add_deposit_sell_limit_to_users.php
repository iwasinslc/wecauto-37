<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDepositSellLimitToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('users', 'deposit_sell_limit')) {
            Schema::table('users', function (Blueprint $table) {
                $table->decimal('deposit_sell_limit', 16,8)->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('users', 'deposit_sell_limit')) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('deposit_sell_limit');
            });
        }
    }
}
